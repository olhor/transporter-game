import Obstacle from "./game/obstacle";
import Player from "./game/player";

var viewport_h, viewport_w;
var SCALE = 30;
var world;
var context, canvas;
var frame = 0;
var startTime;
var fps;
var player;
var timeStep = 1 / 60;
var OBSTACLES_COUNT = 40;
var obstacles = new Array();
var debugDraw;
var RADTODEG = 180 / Math.PI;
var DEGTORAD = Math.PI / 180;
var actions = {32: "THRUST_ACTION", 37: "PITCH_LEFT_ACTION", 39: "PITCH_RIGHT_ACTION"};
var currentActions = {"THRUST_ACTION": false, "PITCH_LEFT_ACTION": false, "PITCH_RIGHT_ACTION": false};

var stage, debugLayer, gameLayer, staticLayer;
var stageW, stageH;

var ais = new Array();
var AIS_COUNT = 5;

var contacting = false;

$(document).ready(function() {
  viewport_w = $('#canvas').width();
  viewport_h = 600;
  console.log("Resolution: "+viewport_w + ", " + viewport_h);
  init();
});

function init() {
  console.log("Game starting...FTW");

  var b2Vec2 = Box2D.Common.Math.b2Vec2,
    b2World = Box2D.Dynamics.b2World,
    b2DebugDraw = Box2D.Dynamics.b2DebugDraw;

  stage = new Kinetic.Stage({
    container: 'canvas',
    width: viewport_w,
    height: viewport_h
  });

  debugLayer = new Kinetic.Layer();
  gameLayer = new Kinetic.Layer();
  staticLayer = new Kinetic.Layer();

  world = new b2World(
    new b2Vec2(0, 10), //gravity
    true                 //allow sleep
  );

  //create obstacles
  for (var i = 0; i < OBSTACLES_COUNT; i++) {
    var obstacle = new Obstacle({
      x: ((Math.random() * 100) - 50),
      y: ((Math.random() * 100) - 50)
    });
    obstacle.addPhysics();
    obstacle.addToWorld(world);
    obstacle.addDrawing(gameLayer, SCALE);
    console.log(obstacle.say('created'));
    obstacles.push(obstacle);
  }

  //create and add player
  player = new Player({name: 'Sławek', x: Math.random() * 40, y: Math.random() * 20, color: 'blue'});
  player.addPhysics();
  if (player.hasPhysics())
    player.addToWorld(world)
  player.addDrawing(gameLayer, SCALE);
  console.log(player.say('created'));

  //create A.I.'s
  for (var i = 0; i < AIS_COUNT; i++) {
    var ai = new Player({name: 'A.I. ' + i, x: ((Math.random() * 100) - 50), y: ((Math.random() * 100) - 50)});
    ai.addPhysics();
    if (ai.hasPhysics())
      ai.addToWorld(world)
    ai.addDrawing(gameLayer, SCALE);
    console.log(ai.say('created'));
    ais.push(ai);
  }

  //add key listeners
  $(document).keydown(function(e) {
    var action = actions[e.which];
    currentActions[action] = true;
  });
  $(document).keyup(function(e) {
    var action = actions[e.which];
    currentActions[action] = false;
  });

  //setup debug draw
  debugDraw = new b2DebugDraw();
  debugDraw.SetSprite(debugLayer.getContext());
  debugDraw.SetDrawScale(SCALE);
  debugDraw.SetFillAlpha(0.3);
  debugDraw.SetLineThickness(1.0);
  debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
  world.SetDebugDraw(debugDraw);

  // add layers to stage
  stage.add(gameLayer);

  stage.add(staticLayer);

  stage.add(debugLayer);

  //start ai's
  for (var i = 0; i < AIS_COUNT; i++) {
    ais[i].automate();
  }
  //console.log(player.body.GetLinearVelocity());
  // start game loop
  startTime = new Date;
  window.setInterval(update, 1000 / 60); // 60 times per second
}

// draw single frame
function update() {
  // respond to user inputs
  respondToActions();
  // iterate physics step
  world.Step(
    timeStep, // time step
    10, //velocity iterations
    10        //position iteratyerions
  );

  // check conacts
  for (var contact = world.GetContactList(); contact != null; contact = contact.GetNext()) {
    var bodyA = contact.GetFixtureA().GetBody();
    var bodyB = contact.GetFixtureB().GetBody();
    if (bodyA == player.body || bodyB == player.body) {
      player.contacting = true;
      break;
    } else {
      player.contacting = false;
    }
  }

  player.updateDrawing(SCALE);
  for (var i = 0; i < AIS_COUNT; i++) {
    ais[i].updateDrawing(SCALE);
  }

  gameLayer.draw();
  gameLayer.setOffsetX(player.body.GetPosition().x * SCALE - (stageW / 2));
  gameLayer.setOffsetY(player.body.GetPosition().y * SCALE - (stageH / 2));

  staticLayer.draw();
  updateDebugData(staticLayer);

  debugLayer.draw();
  debugLayer.getContext().save();
  debugLayer.getContext().translate((stageW / 2) - player.body.GetPosition().x * SCALE, (stageH / 2) - player.body.GetPosition().y * SCALE);
  world.DrawDebugData();
  debugLayer.getContext().restore();

  world.ClearForces();
}

function updateDebugData(layer) {
  var _context = layer.getContext();

  var currTime = new Date;
  if (currTime - startTime > 1000) {
    fps = frame;
    frame = 0;
    startTime = currTime;
  }
  frame++;

  _context.fillStyle = 'white';
  var textLine = 15;
  _context.fillText("FPS " + fps, 5, textLine);
  textLine += 15;
  var pos = player.body.GetPosition();
  _context.fillText("Player position " + Math.round(pos.x) + "," + Math.round(pos.y), 5, textLine);
  textLine += 15;
  _context.fillText("Player angle " + Math.round(player.body.GetAngle() * RADTODEG) % 360, 5, textLine);
  textLine += 15;
  _context.fillText("Angular vel. " + player.body.GetAngularVelocity().toFixed(2), 5, textLine);
  textLine += 15;
  var v = player.body.GetLinearVelocity()
  _context.fillText("Velocity " + player.get('velocity').toFixed(2) + "("+v.x.toFixed(2)+","+v.y.toFixed(2)+")", 5, textLine);
  textLine += 15;
  var tmp = "";
  for (var action in currentActions) {
    if(currentActions[action] === true) tmp += (action + " ");
  }
  _context.fillText("Used actions " + tmp, 5, textLine);
}

// respond to actions
function respondToActions() {
  for (var action in currentActions) {
    if(currentActions[action] === true) { // action is on
      if (action === "THRUST_ACTION") {            // space
        player.thrust();
      } else if (action === "PITCH_LEFT_ACTION") { // cursor left
        player.pitchLeft();
      } else if (action === "PITCH_RIGHT_ACTION") {// cursor right
        player.pitchRight();
      }
    }
  }
}

function startContact() {
  contacting = true;
}
function endContact() {
  contacting = false;
}
