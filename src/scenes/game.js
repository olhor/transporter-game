import Phaser from '../lib/phaser.js'
import dat from '../lib/dat-gui.js'
import Player from '../game/player.js'
import Obstacle from '../game/obstacle.js'
import Package from '../game/package.js'

/**
 * Main game scene.
 */
export default class Game extends Phaser.Scene {
  player
  worldWidth
  worldHeight
  map
  worldScale
  packages
  package
  particleManager
  collisionEmitter
  layer
  fps
  ais

  constructor() {
    super('game')
  }

  init() {
    this.worldScale = 2
    this.packages = []
    this.fps = 0
    this.ais = []
  }

  preload() {
    this.load.image('obstacle', 'assets/obstacle.png') // load obstacle image
    this.load.image('package', 'assets/package.png') // load package image
    // this.load.image('tiles', 'assets/tiles.png') // load tiles image
    this.load.image('player', 'assets/player.png') // load platform image
    this.load.image('ai', 'assets/ai.png') // load platform image
    // this.load.image('particle', 'assets/particle.png')
    this.cursors = this.input.keyboard.createCursorKeys() // add input
    this.spaceKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE)
    this.zKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.Z)
    // load map data
    this.load.tilemapCSV('map', 'assets/maps/small/mayhem.csv')
    this.input.on('pointerdown', this.mouseClick, this)
  }

  mouseClick(pointer) {
    const [x, y] = [pointer.worldX, pointer.worldY]
    const path = new Phaser.Curves.Path(x + 20, y).circleTo(20)
    const hitPoly = new Phaser.Geom.Polygon(path.getPoints(6))
    // draw hit box
    this.g.clear()
    this.g.lineStyle(2, 0xffff00)
    this.g.beginPath()
    this.g.moveTo(hitPoly.points[0].x, hitPoly.points[0].y);
    for (let i = 1; i < hitPoly.points.length; i++) {
      this.g.lineTo(hitPoly.points[i].x, hitPoly.points[i].y);
    }
    this.g.closePath();
    this.g.strokePath();
    // FIXME: get tile, replace  with poly
    const tile = this.map.getTileAtWorldXY(x, y)
    if(tile && tile.index > 0) {
      // console.log('hit!')
      const body = tile.physics.matterBody.body
      let points = body.vertices.map((el) => {
        return new Phaser.Geom.Point(el.x, el.y)
      })
      const initPoly = new Phaser.Geom.Polygon(points)
      // console.log(points)
      let newPoints = []
      // create new polygon clipped by the hit
      // remove all polygon vertices inside hit
      for(let i = 0; i < points.length; i++) {
        const p = points[i]
        const contains = Phaser.Geom.Polygon.ContainsPoint(hitPoly, p)
        if(contains) {
          continue
        }
        newPoints.push(p)
      }
      // add all circle vertices inside polygon as its new vertices
      hitPoly.points.forEach((p) => {
        const contains = Phaser.Geom.Polygon.ContainsPoint(initPoly, p)
        if(contains){
          newPoints.push(p)
        }
      })
      console.log('points', newPoints)
      newPoints = this._sortPolyPoints(newPoints)
      console.log('sort points', newPoints)
      let newPoly = new Phaser.Geom.Polygon(newPoints)
      // console.log('new poly', newPoly)
      // destroy old body
      this.layer.removeTileAt(tile.x, tile.y)
      tile.physics.matterBody.destroy()
      // add new body
      let poly = this.add.polygon(body.position.x, body.position.y, newPoly.points, 0xff0000, 0.5);
      this.matter.add.gameObject(poly, { shape: { type: 'fromVerts', verts: newPoly.points }, isStatic: true });
    }
  }

  create() {
    // load map
    this.map = this.make.tilemap({ key: 'map', tileWidth: 64, tileHeight: 64 })
    // const tileset = this.map.addTilesetImage('tiles')
    // TODO: replace embedded layer builder with obstacles created according to tilemap
    this.layer = this.map.createDynamicLayer(0, null)
    this.layer.setScale(this.worldScale) // FIXME: there is a glitch called 'tile extrusion'
    // set world according to map bounds
    this.worldWidth = this.map.widthInPixels * this.worldScale
    this.worldHeight = this.map.heightInPixels * this.worldScale
    this.matter.world.setBounds(0, 0, this.worldWidth, this.worldHeight)
    // add collisions
    this.map.setCollision(1) // index of tile
    this.map.setCollisionByProperty({ collides: true });
    this.matter.world.convertTilemapLayer(this.layer);

    this._addObstacles(3)
    // add package to be found
    this.package = this._addPackage()

    // add particle system
    this.particleManager = this._createParticles()
    // add collision damage emitter
    this.collisionEmitter = this.particleManager.createEmitter(this._collisionEmitterConfig(this))

    // add player to the scene
    let {x,y} = this._randomFreeTilePosition()
    this.player = new Player(this, x, y, 'player')
    this.add.existing(this.player)

    //add A.I.'s to the scene
    this._addAIs(5)

    // global logic

    // follow camera
    this.cameras.main.startFollow(this.player)
    // set camera bounds
    this.cameras.main.setBounds(0,0,this.worldWidth, this.worldHeight)
    // respond to collisions
    this.matter.world.on('collisionstart', this._handleCollision.bind(this))

    // debugger
    this._createDebugGUI()

    this.g = this.add.graphics()

    console.log(this)
  }

  update(time, dt) {
    this.fps = 1000 / dt
    this._respondToInputs()
    // TODO: add wrapping of multiple objects at once
    this._respondToMapWrap(this.player)
    this._respondToMapWrap(this.package)

    for (let i = 0; i < this.ais.length; i++) {
      let ai = this.ais[i]
      ai.automate()
    }
  }

  // private

  _createDebugGUI() {
    const gui = new dat.GUI()

    const cam = this.cameras.main
    let folder = gui.addFolder("Game")
    folder.add(this, "fps").listen()

    folder = gui.addFolder('Camera')
    folder.add(cam.midPoint, 'x').listen()
    folder.add(cam.midPoint, 'y').listen()
    folder.add(cam, 'scrollX').listen()
    folder.add(cam, 'scrollY').listen()

    folder = gui.addFolder('Player')
    folder.add(this.player.body, 'speed', null, null, 0.001).listen()
    folder.add(this.player.body, 'angle', null, null, 0.001).listen()
    folder.add(this.player, 'x').listen()
    folder.add(this.player, 'y').listen()

    folder = gui.addFolder('Package')
    folder.add(this.package.body, 'speed', null, null, 0.001).listen()
    folder.add(this.package.body, 'angle', null, null, 0.001).listen()
    folder.add(this.package, 'x').listen()
    folder.add(this.package, 'y').listen()

    for (let i = 0; i < this.ais.length; i++) {
      let ai = this.ais[i]
      
      folder = gui.addFolder('AI '+i)
      folder.add(ai.body, 'speed', null, null, 0.001).listen()
      folder.add(ai.body, 'angle', null, null, 0.001).listen()
      folder.add(ai, 'x').listen()
      folder.add(ai, 'y').listen()
    }

  }

  // @deprecated
  _updateDebugData(layer) {
    var _context = layer.getContext();
  
    var currTime = new Date;
    if (currTime - startTime > 1000) {
      fps = frame;
      frame = 0;
      startTime = currTime;
    }
    frame++;
  
    _context.fillStyle = 'white';
    var textLine = 15;
    _context.fillText("FPS " + fps, 5, textLine);
    textLine += 15;
    var pos = player.body.GetPosition();
    _context.fillText("Player position " + Math.round(pos.x) + "," + Math.round(pos.y), 5, textLine);
    textLine += 15;
    _context.fillText("Player angle " + Math.round(player.body.GetAngle() * RADTODEG) % 360, 5, textLine);
    textLine += 15;
    _context.fillText("Angular vel. " + player.body.GetAngularVelocity().toFixed(2), 5, textLine);
    textLine += 15;
    var v = player.body.GetLinearVelocity()
    _context.fillText("Velocity " + player.get('velocity').toFixed(2) + "("+v.x.toFixed(2)+","+v.y.toFixed(2)+")", 5, textLine);
    textLine += 15;
    var tmp = "";
    for (var action in currentActions) {
      if(currentActions[action] === true) tmp += (action + " ");
    }
    _context.fillText("Used actions " + tmp, 5, textLine);
  }

  _addAIs(count) {
    for (let i = 0; i < count; i++) {
      let {x,y} = this._randomFreeTilePosition()
      let ai = new Player(this, x, y, 'ai')
      // ai.automate()
      this.ais.push(ai)
      this.add.existing(ai)
    }
  }

  _addObstacles(count) {
    for (let i = 0; i < count; i++) {
      // const x = Phaser.Math.Between(0, this.worldWidth)
      // const y = Phaser.Math.Between(0, this.worldHeight)
      const {x,y} = this._randomFreeTilePosition()
      const obstacle = new Obstacle(this, x, y, 'obstacle')
      this.add.existing(obstacle)
    }
  }

  // sort polygon points
  _sortPolyPoints(points) {
    const centroid = Phaser.Geom.Point.GetCentroid(points);
    let angles = []
    points.forEach((p) => {
      const angle = Phaser.Math.Angle.BetweenPoints(centroid, p)
      angles.push(angle)
    })
    console.log(angles)
    // selection sort
    for(let i = 0; i < angles.length; i++) {
      let jMin = i
      // find min angle
      for(let j = i + 1; j < angles.length; j++) {
        if(angles[j] < angles[jMin]) {
          jMin = j
        }
      }
      // update angles array
      let el = angles[jMin]
      angles.splice(jMin, 1)
      angles.unshift(el)
      // update points array
      el = points[jMin]
      points.splice(jMin, 1)
      points.unshift(el)
    }
    return points
  }
  /**
   * Adds randomly positioned packages at free tiles.
   * @param count
   * @returns {*}
   * @private
   */
  _addPackages(count) {
    for (let i = 0; i < count; i++) {
      let {x, y} = this._randomFreeTilePosition()
      console.log('package at', x, y)
      const pack = new Package(this, x, y, 'package')
      this.packages.push(pack)
      this.add.existing(pack)
    }
    return this.packages
  }

  _addPackage(x = null, y = null) {
    let pack
    if(x === null || y === null)
      ({x, y} = this._randomFreeTilePosition())
    pack = new Package(this, x, y, 'package')
    console.log('package at', x, y)
    this.add.existing(pack)
    return pack
  }

  _randomFreeTilePosition() {
    let x, y, tile
    let hasTile = true
    while(hasTile) {
      x = Phaser.Math.Between(0, this.worldWidth)
      y = Phaser.Math.Between(0, this.worldHeight)
      // let tiles = this.map.getTilesWithinWorldXY(x, y, 1, 1)
      tile = this.map.getTileAtWorldXY(x, y)
      hasTile = tile.index !== 0
    }
    return {x: x, y: y}
  }

  _respondToInputs() {
    if(this.cursors.left.isDown) {
      this.player.pitchLeft()
    } else if(this.cursors.right.isDown) {
      this.player.pitchRight()
    }
    if(this.spaceKey.isDown) {
      this.player.doThrust()
    } else {
      this.player.stopThrust()
    }
    if(this.zKey.isUp) {
      this.zKeyReleased = true
    } else {
      if(this.zKeyReleased) {
        if(this.zKey.isDown && !this.player.holding) {
          this.player.grabPackage(this.package)
        } else if(this.zKey.isDown && this.player.holding) {
          this.player.releasePackage(this.package)
        }
        this.zKeyReleased = false
      }
    }
  }

  /**
   * Wraps single sprite around the map borders.
   * @param sprite
   * @private
   */
  _respondToMapWrap(sprite) {
    const tileSize = this.map.tileWidth * this.worldScale
    if(sprite.x < tileSize) { // left to right
      sprite.x = this.worldWidth - tileSize
    } else if(sprite.x > this.worldWidth - tileSize) { // right to left
      sprite.x = tileSize
    } else if(sprite.y < tileSize) { // top to bottom
      sprite.y = this.worldHeight - tileSize
    } else if(sprite.y > this.worldHeight - tileSize) { // bottom to top
      sprite.y = tileSize
    }
  }

  _createParticles() {
    // create particle image
    const graphics = new Phaser.GameObjects.Graphics(this) // create graphics object
    graphics.fillStyle(0xFFFFFF)
    graphics.fillRect(0, 0, 16, 16);
    graphics.generateTexture('particle', 16, 16) // generate texture from graphics object using provided key
    return this.add.particles('particle')
  }

  _collisionEmitterConfig(ship) {
    return {
      scale: {start: 0.8, end: 0},
      lifespan: 500,
      speed: 10,
      frequency: -1
    }
  }

  _handleCollision(event, bodyA, bodyB) {
    // console.log('collision', event)
    // get exact collision point in first body
    const lists = event.source.pairs.list
    lists.forEach((list) => {
      const contacts = list.activeContacts
      contacts.forEach((contact) => {
        const x = contact.vertex.x
        const y = contact.vertex.y
        this.collisionEmitter.emitParticle(10, x, y)
      })
    })
    // TODO: find which is better
    // const supports = list.collision.supports
    // supports.forEach((support) => {
    //   const x = support.x
    //   const y = support.y
    //   this.collisionEmitter.emitParticle(10, x, y)
    // })
  }
}
