import Phaser from './lib/phaser.js'
import Game from './scenes/game.js'

export default new Phaser.Game({
    type: Phaser.AUTO,
    width: 640,
    height: 640,
    scene: [Game],
    physics: {
        default: 'matter',
        matter: {
            gravity: {
                y: 0.3
            },
            debug: true
        }
    }
})
