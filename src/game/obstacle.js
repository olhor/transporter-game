import Phaser from '../lib/phaser.js'

export default class Obstacle extends Phaser.Physics.Matter.Sprite {
    constructor(scene, x, y, texture) {
        super(scene.matter.world, x, y, texture)

        this.setStatic(true)
    }

    say(message) {
        console.log('obstacle', message)
    }
}
