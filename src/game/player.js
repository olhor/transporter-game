import Phaser from '../lib/phaser.js'

export default class Player extends Phaser.Physics.Matter.Sprite {
  burstEmitter
  thrusting
  colliding
  holding

  constructor(scene, x, y, texture) {
    super(scene.matter.world, x, y, texture)

    this.scene = scene
    const halfWidth = this.displayWidth * 0.5
    const halfHeight = this.displayHeight * 0.5
    const verts = [
      new Phaser.Math.Vector2(-halfWidth, 0),
      new Phaser.Math.Vector2(-halfWidth + 10, -halfHeight),
      new Phaser.Math.Vector2(halfWidth - 10, -halfHeight),
      new Phaser.Math.Vector2(halfWidth, 0),
      new Phaser.Math.Vector2(halfWidth - 10, halfHeight),
      new Phaser.Math.Vector2(-halfWidth + 10, halfHeight)
    ]
    this.setBody({
      type: 'fromVerts',
      verts: verts
    })

    // add thruster burst emitter
    this.burstEmitter = scene.particleManager.createEmitter(this._burstEmitterConfig(this))

    // other logic
    this.holding = false
  }

  say(message) {
    console.log('player', this.name, message)
  }

  doThrust() {
    this.thrusting = true
    const impulse = 0.002
    const angle = this.body.angle
    const angleOffset = 90 * Math.PI / 180
    const force = new Phaser.Math.Vector2(Math.cos(angle - angleOffset) * impulse, Math.sin(angle - angleOffset) * impulse)

    this.applyForce(force)
    // drop down angular velocity
    const angVel = this.body.angularVelocity
    if(angVel > 0) {
      this.setAngularVelocity(angVel - 0.0002)
    }
    if(angVel < 0) {
      this.setAngularVelocity(angVel + 0.0002)
    }
    const thruster = this._burstPosition()
    this.burstEmitter.emitParticle(1, thruster.x, thruster.y)
  }

  pitchRight() {
    const angVel = this.body.angularVelocity
    this.setAngularVelocity(angVel + 0.002)
  }

  pitchLeft() {
    const angVel = this.body.angularVelocity
    this.setAngularVelocity(angVel - 0.002)
  }

  stopThrust() {
    this.thrusting = false
  }

  /* same as this.body.speed */
  linearVelocity() {
    return Math.sqrt(Math.pow(this.body.velocity.x, 2) + Math.pow(this.body.velocity.y, 2))
  }

  startColliding() {
    this.colliding = true
  }

  stopColliding() {
    this.colliding = false
  }

  releasePackage(pack) {
    this.scene.matter.world.removeConstraint(this.rope)
    this.holding = false
  }

  grabPackage(pack) {
    const distance = Phaser.Math.Distance.Between(this.x, this.y, pack.x, pack.y)
    console.log('distance', distance)
    if(distance <= 120) {
      // TODO: create rope from multiple constraints
      this.rope = this.scene.matter.add.constraint(this.body, pack.body, 100)
      this.holding = true
    }
  }

  automate() {
		let vel = this.body.velocity
		let angle = this.body.angle
		// simple AI
		if(vel.y > 0) {
			this.doThrust()			
		} else {
      this.stopThrust()
    }		
		if(angle > 0) {
			this.pitchLeft();
		} else {
			this.pitchRight();
		}
	}

  // private

  _burstEmitterConfig(ship) {
    return {
      x: 0,
      y: 0,
      scale: {start: 0.4, end: 0},
      angle: {
        onEmit: function(particle, key, t, value) {
          const angle = Phaser.Math.RadToDeg(ship.body.angle)
          return angle + 90 + Phaser.Math.Between(0, 10) - 5
        }
      },
      speed: {
        onEmit: function(particle, key, t, value) {
          return ship.body.speed * 50
        }
      },
      lifespan: {start: 800, end: 400},
      frequency: -1, // set in explode mode (do not emit automatically)
      blendMode: 'ADD'
    }
  }

  _burstPosition() {
    let object = {}
    const halfHeight = this.displayHeight * 0.5
    const angle = this.body.angle
    const angleOffset = Phaser.Math.DegToRad(90)
    object.x = this.x + Math.cos(angle + angleOffset) * halfHeight
    object.y = this.y + Math.sin(angle + angleOffset) * halfHeight
    return object
  }
}
