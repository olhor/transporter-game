import Phaser from '../lib/phaser.js'

export default class Package extends Phaser.Physics.Matter.Sprite {
    constructor(scene, x, y, texture) {
        super(scene.matter.world, x, y, texture)
        this.setScale(0.5)
    }

    say(message) {
        console.log('package', message)
    }
}
